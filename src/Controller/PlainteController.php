<?php

namespace App\Controller;

use App\Entity\Plainte;
use App\Form\PlainteType;
use DiscordWebhooks\Embed;
use DiscordWebhooks\Client;
use App\Repository\PlainteRepository;
use App\Service\DiscordWebhookService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/plainte")
 */
class PlainteController extends AbstractController
{


    /**
     * @Route("/", name="plainte_index", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(PlainteRepository $plainteRepository): Response
    {
        return $this->render('plainte/index.html.twig', [
            'plaintes' => $plainteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="plainte_new", methods={"GET","POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function new(Request $request, DiscordWebhookService $discord): Response
    {
        $plainte = new Plainte();
        $form = $this->createForm(PlainteType::class, $plainte);
        $form->handleRequest($request);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            if ($user->getGroupe() != null){
            $plainte->setGroupe($user->getGroupe());
            }
            $entityManager->persist($plainte);
            $entityManager->flush();

            $webhook = $user->getpWebhook();

            if ($webhook != null ) {
                $content = "Nouvelle Plainte ajouter par : ".$plainte->getAgent();
                $title = "Nouvelle plainte ajouter";
                $discord->sendWebhook($webhook,$content, $title);
            }

            return $this->redirectToRoute('plainte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('plainte/new.html.twig', [
            'plainte' => $plainte,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="plainte_show", methods={"GET"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function show(Plainte $plainte): Response
    {
        return $this->render('plainte/show.html.twig', [
            'plainte' => $plainte,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="plainte_edit", methods={"GET","POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function edit(Request $request, Plainte $plainte): Response
    {
        $form = $this->createForm(PlainteType::class, $plainte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('plainte_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('plainte/edit.html.twig', [
            'plainte' => $plainte,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="plainte_delete", methods={"POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function delete(Request $request, Plainte $plainte): Response
    {
        if ($this->isCsrfTokenValid('delete'.$plainte->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($plainte);
            $entityManager->flush();
        }

        return $this->redirectToRoute('plainte_index', [], Response::HTTP_SEE_OTHER);
    }
}
