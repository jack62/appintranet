<?php

namespace App\Form;

use App\Entity\Casier;
use App\Entity\Plainte;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PlainteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agent', TextType::class)
            ->add('casier', EntityType::class, [
                // looks for choices from this entity
                'class' => Casier::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
            
                // used to render a select box, check boxes or radios
                 'multiple' => false,
                'expanded' => false,
            ])
            ->add('heureDuDepot', TimeType::class)
            ->add('nomPlaignant', TextType::class)
            ->add('numeroPlaignant', TextType::class)
            ->add('lieuDesFait', TextType::class)
            ->add('plainte', CKEditorType::class)
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Plainte::class,
        ]);
    }
}
