<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

     /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $grade;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $service;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre mot de passe doit étre de minimun 8 caractere")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $groupe;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pWebhook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $rWebhook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cWebhook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sWebhook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $vWebhook;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $aWebhook;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_MEMBER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    public function eraseCredentials()
    {
    }
    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function __toString()
    {
    }

    public function getGroupe(): ?string
    {
        return $this->groupe;
    }

    public function setGroupe(?string $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getPWebhook(): ?string
    {
        return $this->pWebhook;
    }

    public function setPWebhook(?string $pWebhook): self
    {
        $this->pWebhook = $pWebhook;

        return $this;
    }

    public function getRWebhook(): ?string
    {
        return $this->rWebhook;
    }

    public function setRWebhook(?string $rWebhook): self
    {
        $this->rWebhook = $rWebhook;

        return $this;
    }

    public function getCWebhook(): ?string
    {
        return $this->cWebhook;
    }

    public function setCWebhook(?string $cWebhook): self
    {
        $this->cWebhook = $cWebhook;

        return $this;
    }

    public function getSWebhook(): ?string
    {
        return $this->sWebhook;
    }

    public function setSWebhook(?string $sWebhook): self
    {
        $this->sWebhook = $sWebhook;

        return $this;
    }

    public function getVWebhook(): ?string
    {
        return $this->vWebhook;
    }

    public function setVWebhook(?string $vWebhook): self
    {
        $this->vWebhook = $vWebhook;

        return $this;
    }

    public function getAWebhook(): ?string
    {
        return $this->aWebhook;
    }

    public function setAWebhook(?string $aWebhook): self
    {
        $this->aWebhook = $aWebhook;

        return $this;
    }
}
