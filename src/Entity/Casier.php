<?php

namespace App\Entity;

use App\Repository\CasierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CasierRepository::class)
 */
class Casier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateDeNaissance;

    /**
     * @ORM\OneToMany(targetEntity=plainte::class, mappedBy="casier")
     */
    private $rapport;

    /**
     * @ORM\OneToMany(targetEntity=Saisie::class, mappedBy="casier")
     */
    private $saisie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $agentCreateur;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $partage;

    public function __construct()
    {
        $this->rapport = new ArrayCollection();
        $this->saisie = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateDeNaissance(): ?\DateTimeInterface
    {
        return $this->dateDeNaissance;
    }

    public function setDateDeNaissance(?\DateTimeInterface $dateDeNaissance): self
    {
        $this->dateDeNaissance = $dateDeNaissance;

        return $this;
    }

    /**
     * @return Collection|plainte[]
     */
    public function getRapport(): Collection
    {
        return $this->rapport;
    }

    public function addRapport(plainte $rapport): self
    {
        if (!$this->rapport->contains($rapport)) {
            $this->rapport[] = $rapport;
            $rapport->setCasier($this);
        }

        return $this;
    }

    public function removeRapport(plainte $rapport): self
    {
        if ($this->rapport->removeElement($rapport)) {
            // set the owning side to null (unless already changed)
            if ($rapport->getCasier() === $this) {
                $rapport->setCasier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|saisie[]
     */
    public function getSaisie(): Collection
    {
        return $this->saisie;
    }

    public function addSaisie(saisie $saisie): self
    {
        if (!$this->saisie->contains($saisie)) {
            $this->saisie[] = $saisie;
            $saisie->setCasier($this);
        }

        return $this;
    }

    public function removeSaisie(saisie $saisie): self
    {
        if ($this->saisie->removeElement($saisie)) {
            // set the owning side to null (unless already changed)
            if ($saisie->getCasier() === $this) {
                $saisie->setCasier(null);
            }
        }

        return $this;
    }

    public function getAgentCreateur(): ?string
    {
        return $this->agentCreateur;
    }

    public function setAgentCreateur(string $agentCreateur): self
    {
        $this->agentCreateur = $agentCreateur;

        return $this;
    }

    public function getPartage(): ?bool
    {
        return $this->partage;
    }

    public function setPartage(?bool $partage): self
    {
        $this->partage = $partage;

        return $this;
    }
}
