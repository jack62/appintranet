<?php

namespace App\Entity;

use App\Repository\PlainteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlainteRepository::class)
 */
class Plainte
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $agent;

    /**
     * @ORM\Column(type="datetime")
     */
    private $heureDuDepot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomPlaignant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numeroPlaignant;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lieuDesFait;

    /**
     * @ORM\Column(type="text")
     */
    private $plainte;

    /**
     * @ORM\ManyToOne(targetEntity=Casier::class, inversedBy="rapport")
     */
    private $casier;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $groupe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgent(): ?string
    {
        return $this->agent;
    }

    public function setAgent(string $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getHeureDuDepot(): ?\DateTimeInterface
    {
        return $this->heureDuDepot;
    }

    public function setHeureDuDepot(\DateTimeInterface $heureDuDepot): self
    {
        $this->heureDuDepot = $heureDuDepot;

        return $this;
    }

    public function getNomPlaignant(): ?string
    {
        return $this->nomPlaignant;
    }

    public function setNomPlaignant(string $nomPlaignant): self
    {
        $this->nomPlaignant = $nomPlaignant;

        return $this;
    }

    public function getNumeroPlaignant(): ?string
    {
        return $this->numeroPlaignant;
    }

    public function setNumeroPlaignant(string $numeroPlaignant): self
    {
        $this->numeroPlaignant = $numeroPlaignant;

        return $this;
    }

    public function getLieuDesFait(): ?string
    {
        return $this->lieuDesFait;
    }

    public function setLieuDesFait(?string $lieuDesFait): self
    {
        $this->lieuDesFait = $lieuDesFait;

        return $this;
    }

    public function getPlainte(): ?string
    {
        return $this->plainte;
    }

    public function setPlainte(string $plainte): self
    {
        $this->plainte = $plainte;

        return $this;
    }

    public function getCasier(): ?Casier
    {
        return $this->casier;
    }

    public function setCasier(?Casier $casier): self
    {
        $this->casier = $casier;

        return $this;
    }

    public function getGroupe(): ?string
    {
        return $this->groupe;
    }

    public function setGroupe(string $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }
}
