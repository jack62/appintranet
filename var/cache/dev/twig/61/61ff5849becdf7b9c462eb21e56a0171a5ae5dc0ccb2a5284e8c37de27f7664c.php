<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* includes/menu.html.twig */
class __TwigTemplate_69e44d479879290a8491202367c389d1b479f0261ffac06b5761c1e5e79fd05a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "includes/menu.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "includes/menu.html.twig"));

        // line 1
        echo " <section id=\"container\">
 <header class=\"header black-bg\">
      <div class=\"sidebar-toggle-box\">
        <div class=\"fa fa-bars tooltips\" data-placement=\"right\" data-original-title=\"Toggle Navigation\"></div>
      </div>
      <!--logo start-->
      <a href=\"index.html\" class=\"logo\"><b>NEO<span>SEARCH</span></b></a>
      <!--logo end-->
      <div class=\"nav notify-row\" id=\"top_menu\">
      </div>
      <div class=\"top-menu\">
        <ul class=\"nav pull-right top-menu\">
        ";
        // line 13
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 13), "username", [], "any", true, true, false, 13)) {
            // line 14
            echo "          
          <li><a class=\"logout\" href=\"";
            // line 15
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_logout");
            echo "\">Déconnexion</a></li>
        ";
        }
        // line 17
        echo "        </ul>
      </div>
    </header>
 <aside>
      <div id=\"sidebar\" class=\"nav-collapse \">
        <!-- sidebar menu start-->
        <ul class=\"sidebar-menu\" id=\"nav-accordion\">
          <p class=\"centered\">";
        // line 24
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "user", [], "any", false, false, false, 24), "service", [], "any", false, false, false, 24), "LSPD"))) {
            echo "<img src=\"https://prod.cloud.rockstargames.com/crews/sc/5282/44515541/publish/emblem/emblem_512.png\" class=\"img-circle\" width=\"80\">";
        } else {
            echo "<img src=\"https://pbs.twimg.com/profile_images/1119312498182635520/5GCcoJ7f.png\" class=\"img-circle\" width=\"80\">";
        }
        echo "</p> 
          <h5 class=\"centered\">";
        // line 25
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "service", [], "any", false, false, false, 25), "LSPD"))) {
            echo " <strong style=\"color:#7474eb\">LSPD ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "matricule", [], "any", false, false, false, 25), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "username", [], "any", false, false, false, 25), "html", null, true);
            echo "<br/><p style=\"font-size:0.8em;\">ID du groupe utilisateur : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "groupe", [], "any", false, false, false, 25), "html", null, true);
            echo "</p><strong> ";
        } else {
            echo "<strong style=\"color:#15e885\">BCSO ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "matricule", [], "any", false, false, false, 25), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "username", [], "any", false, false, false, 25), "html", null, true);
            echo "<strong><br/><p style=\"font-size:0.8em;\">ID du groupe utilisateur : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 25, $this->source); })()), "user", [], "any", false, false, false, 25), "groupe", [], "any", false, false, false, 25), "html", null, true);
            echo "</p>";
        }
        echo "</h5> 
          ";
        // line 26
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
            // line 27
            echo "            <li class=\"sub-menu\">
              <a href=\"javascript:;\">
                <i class=\"fa fa-legal\"></i>
                <span>Administration</span>
                </a>
              <ul class=\"sub\">
                <li><a href=\"general.html\"><i class=\"fa fa-user\"></i> Crée un utilisateur</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-user\"></i> Lister les utilisateurs</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-pencil\"></i>Note de service</a></li>
              </ul>
            </li>
          ";
        }
        // line 39
        echo "          
          <li class=\"mt\">
            <a class=\"active\" href=\"index.html\">
              <i class=\"fa fa-dashboard\"></i>
              <span>Information service</span>
              </a>
          </li>

          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-legal\"></i>
              <span>Casier Judiciaire</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
              <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-briefcase\"></i>
              <span>Plaintes</span>
              </a>
            <ul class=\"sub\">
                <li><a href=\"";
        // line 63
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_index");
        echo "\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"";
        // line 64
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_new");
        echo "\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-book\"></i>
              <span>Rapport Interventions</span>
              </a>
            <ul class=\"sub\">
               <li><a href=\"";
        // line 73
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rapport_index");
        echo "\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"";
        // line 74
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rapport_new");
        echo "\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-tasks\"></i>
              <span>Saisie</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-th\"></i>
              <span>Avis de Recherche</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "includes/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 74,  162 => 73,  150 => 64,  146 => 63,  120 => 39,  106 => 27,  104 => 26,  84 => 25,  76 => 24,  67 => 17,  62 => 15,  59 => 14,  57 => 13,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" <section id=\"container\">
 <header class=\"header black-bg\">
      <div class=\"sidebar-toggle-box\">
        <div class=\"fa fa-bars tooltips\" data-placement=\"right\" data-original-title=\"Toggle Navigation\"></div>
      </div>
      <!--logo start-->
      <a href=\"index.html\" class=\"logo\"><b>NEO<span>SEARCH</span></b></a>
      <!--logo end-->
      <div class=\"nav notify-row\" id=\"top_menu\">
      </div>
      <div class=\"top-menu\">
        <ul class=\"nav pull-right top-menu\">
        {% if app.user.username is defined %}
          
          <li><a class=\"logout\" href=\"{{ path('security_logout')}}\">Déconnexion</a></li>
        {% endif %}
        </ul>
      </div>
    </header>
 <aside>
      <div id=\"sidebar\" class=\"nav-collapse \">
        <!-- sidebar menu start-->
        <ul class=\"sidebar-menu\" id=\"nav-accordion\">
          <p class=\"centered\">{% if app.user.service == \"LSPD\" %}<img src=\"https://prod.cloud.rockstargames.com/crews/sc/5282/44515541/publish/emblem/emblem_512.png\" class=\"img-circle\" width=\"80\">{% else %}<img src=\"https://pbs.twimg.com/profile_images/1119312498182635520/5GCcoJ7f.png\" class=\"img-circle\" width=\"80\">{% endif %}</p> 
          <h5 class=\"centered\">{% if app.user.service == \"LSPD\" %} <strong style=\"color:#7474eb\">LSPD {{ app.user.matricule }}-{{ app.user.username }}<br/><p style=\"font-size:0.8em;\">ID du groupe utilisateur : {{ app.user.groupe }}</p><strong> {% else %}<strong style=\"color:#15e885\">BCSO {{ app.user.matricule }}-{{ app.user.username }}<strong><br/><p style=\"font-size:0.8em;\">ID du groupe utilisateur : {{ app.user.groupe }}</p>{% endif %}</h5> 
          {% if is_granted('ROLE_ADMIN') %}
            <li class=\"sub-menu\">
              <a href=\"javascript:;\">
                <i class=\"fa fa-legal\"></i>
                <span>Administration</span>
                </a>
              <ul class=\"sub\">
                <li><a href=\"general.html\"><i class=\"fa fa-user\"></i> Crée un utilisateur</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-user\"></i> Lister les utilisateurs</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-pencil\"></i>Note de service</a></li>
              </ul>
            </li>
          {% endif %}
          
          <li class=\"mt\">
            <a class=\"active\" href=\"index.html\">
              <i class=\"fa fa-dashboard\"></i>
              <span>Information service</span>
              </a>
          </li>

          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-legal\"></i>
              <span>Casier Judiciaire</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
              <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-briefcase\"></i>
              <span>Plaintes</span>
              </a>
            <ul class=\"sub\">
                <li><a href=\"{{ path('plainte_index')}}\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"{{ path('plainte_new')}}\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-book\"></i>
              <span>Rapport Interventions</span>
              </a>
            <ul class=\"sub\">
               <li><a href=\"{{ path('rapport_index')}}\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"{{ path('rapport_new')}}\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-tasks\"></i>
              <span>Saisie</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
          <li class=\"sub-menu\">
            <a href=\"javascript:;\">
              <i class=\"fa fa-th\"></i>
              <span>Avis de Recherche</span>
              </a>
            <ul class=\"sub\">
              <li><a href=\"general.html\"><i class=\"fa fa-search\"></i> Voir</a></li>
                <li><a href=\"buttons.html\"><i class=\"fa fa-file-text-o\"></i> Créer</a></li>
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>", "includes/menu.html.twig", "C:\\appintranet\\templates\\includes\\menu.html.twig");
    }
}
