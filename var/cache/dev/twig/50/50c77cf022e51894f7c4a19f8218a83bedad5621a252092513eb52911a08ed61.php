<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* plainte/_form.html.twig */
class __TwigTemplate_680e9e48f5ce9aa6b195adc02811bbf15aacdab4057711b70f3ab7d41e086fa6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "plainte/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "plainte/_form.html.twig"));

        // line 1
        echo "<div class=\"modifywidth\">
";
        // line 2
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 2, $this->source); })()), 'form_start');
        echo "

    <div class=\"form-group\">
     ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "agent", [], "any", false, false, false, 5), 'label', ["label" => "Nom de l'agent rédacteur"]);
        // line 7
        echo "
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "agent", [], "any", false, false, false, 8), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>

    <div class=\"form-group\">
    ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "heureDuDepot", [], "any", false, false, false, 12), 'label', ["label" => "Heure du Dépôt de plainte"]);
        // line 14
        echo "
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "heureDuDepot", [], "any", false, false, false, 15), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
         ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "nomPlaignant", [], "any", false, false, false, 18), 'label', ["label" => "Nom du plaignant"]);
        // line 20
        echo "
        ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "nomPlaignant", [], "any", false, false, false, 21), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
         ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "numeroPlaignant", [], "any", false, false, false, 24), 'label', ["label" => "Numéro du plaigant"]);
        // line 26
        echo "
        ";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "numeroPlaignant", [], "any", false, false, false, 27), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
         ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), "lieuDesFait", [], "any", false, false, false, 30), 'label', ["label" => "Lieu ou c'est passer les fait ?"]);
        // line 32
        echo "
        ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "lieuDesFait", [], "any", false, false, false, 33), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>

     <div class=\"form-group\">
         ";
        // line 37
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })()), "casier", [], "any", false, false, false, 37), 'label', ["label" => "Si le suspect est connue des service lié le ici à sont casier  ?"]);
        // line 39
        echo "
        ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), "casier", [], "any", false, false, false, 40), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <div class=\"form-group\">
         ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), "plainte", [], "any", false, false, false, 43), 'label', ["label" => "Rédaction de la plainte "]);
        // line 45
        echo "
        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 46, $this->source); })()), "plainte", [], "any", false, false, false, 46), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
    </div>
    <button class=\"btn btn-success\" style=\"margin: 10px;\">";
        // line 48
        echo twig_escape_filter($this->env, ((array_key_exists("button_label", $context)) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 48, $this->source); })()), "SAUVEGARDER")) : ("SAUVEGARDER")), "html", null, true);
        echo "</button>

";
        // line 50
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), 'form_end');
        echo "

    <div style=\"display:flex; align-items:center; justify-content: center; margin: 15px;\">
     
    <a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_index");
        echo "\"><button class=\"btn btn-warning\" style=\"margin: 10px;\">Retour à la liste</button></a>
   
    </div>
    </div>


";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "plainte/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 54,  135 => 50,  130 => 48,  125 => 46,  122 => 45,  120 => 43,  114 => 40,  111 => 39,  109 => 37,  102 => 33,  99 => 32,  97 => 30,  91 => 27,  88 => 26,  86 => 24,  80 => 21,  77 => 20,  75 => 18,  69 => 15,  66 => 14,  64 => 12,  57 => 8,  54 => 7,  52 => 5,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"modifywidth\">
{{ form_start(form) }}

    <div class=\"form-group\">
     {{ form_label(form.agent, null, {
    'label': 'Nom de l\\'agent rédacteur'
}) }}
        {{ form_widget(form.agent, {'attr': {'class': 'form-control'}}) }}
    </div>

    <div class=\"form-group\">
    {{ form_label(form.heureDuDepot, null, {
    'label': 'Heure du Dépôt de plainte'
}) }}
        {{ form_widget(form.heureDuDepot, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
         {{ form_label(form.nomPlaignant, null, {
    'label': 'Nom du plaignant'
}) }}
        {{ form_widget(form.nomPlaignant, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
         {{ form_label(form.numeroPlaignant, null, {
    'label': 'Numéro du plaigant'
}) }}
        {{ form_widget(form.numeroPlaignant, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
         {{ form_label(form.lieuDesFait, null, {
    'label': 'Lieu ou c\\'est passer les fait ?'
}) }}
        {{ form_widget(form.lieuDesFait, {'attr': {'class': 'form-control'}}) }}
    </div>

     <div class=\"form-group\">
         {{ form_label(form.casier, null, {
    'label': 'Si le suspect est connue des service lié le ici à sont casier  ?'
}) }}
        {{ form_widget(form.casier, {'attr': {'class': 'form-control'}}) }}
    </div>
    <div class=\"form-group\">
         {{ form_label(form.plainte, null, {
    'label': 'Rédaction de la plainte '
}) }}
        {{ form_widget(form.plainte, {'attr': {'class': 'form-control'}}) }}
    </div>
    <button class=\"btn btn-success\" style=\"margin: 10px;\">{{ button_label|default('SAUVEGARDER') }}</button>

{{ form_end(form) }}

    <div style=\"display:flex; align-items:center; justify-content: center; margin: 15px;\">
     
    <a href=\"{{ path('plainte_index') }}\"><button class=\"btn btn-warning\" style=\"margin: 10px;\">Retour à la liste</button></a>
   
    </div>
    </div>


", "plainte/_form.html.twig", "C:\\appintranet\\templates\\plainte\\_form.html.twig");
    }
}
