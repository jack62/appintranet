<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* plainte/index.html.twig */
class __TwigTemplate_435a4fc460c943277758503874f64f39aa59d5b85f1c2bf447db6a37fc66ce45 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "plainte/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "plainte/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "plainte/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Plainte";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "        <h3><i class=\"fa fa-angle-right\"></i> <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_new");
        echo "\"><button class=\"btn btn-success\">Nouvelle plainte</button></a></h3>
        <div class=\"row mt\">
          <div class=\"col-lg-12\">
            <div class=\"content-panel\">
             
              <section id=\"unseen\">
<table class=\"table table-bordered table-striped table-condensed\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Agent</th>
                <th>HeureDuDepot</th>
                <th>NomPlaignant</th>
                <th>NumeroPlaignant</th>
                <th>LieuDesFait</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["plaintes"]) || array_key_exists("plaintes", $context) ? $context["plaintes"] : (function () { throw new RuntimeError('Variable "plaintes" does not exist.', 25, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["plainte"]) {
            // line 26
            echo "            ";
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26), "groupe", [], "any", false, false, false, 26), twig_get_attribute($this->env, $this->source, $context["plainte"], "groupe", [], "any", false, false, false, 26)))) {
                // line 27
                echo "                <tr>
                    <td>";
                // line 28
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "id", [], "any", false, false, false, 28), "html", null, true);
                echo "</td>
                    <td>";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "agent", [], "any", false, false, false, 29), "html", null, true);
                echo "</td>
                    <td>";
                // line 30
                ((twig_get_attribute($this->env, $this->source, $context["plainte"], "heureDuDepot", [], "any", false, false, false, 30)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "heureDuDepot", [], "any", false, false, false, 30), "H:i:s"), "html", null, true))) : (print ("")));
                echo "</td>
                    <td>";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "nomPlaignant", [], "any", false, false, false, 31), "html", null, true);
                echo "</td>
                    <td>";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "numeroPlaignant", [], "any", false, false, false, 32), "html", null, true);
                echo "</td>
                    <td>";
                // line 33
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["plainte"], "lieuDesFait", [], "any", false, false, false, 33), "html", null, true);
                echo "</td>
                    <td>
                        <a href=\"";
                // line 35
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_show", ["id" => twig_get_attribute($this->env, $this->source, $context["plainte"], "id", [], "any", false, false, false, 35)]), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-warning\"><i class=\"fas fa-edit\"></i></button></a>
                        <a href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("plainte_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["plainte"], "id", [], "any", false, false, false, 36)]), "html", null, true);
                echo "\"><button type=\"button\" class=\"btn btn-primary\"><i class=\"far fa-eye\"></i></button></a>
                    </td>
                </tr>
        ";
            }
            // line 40
            echo "        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 41
            echo "            <tr>
                <td colspan=\"8\">Aucune plainte pour le moment</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['plainte'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </tbody>
    </table>

    
     </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        </div>

   
     </section>
 <!--main content end-->
    <!--footer start-->
 
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "plainte/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 45,  163 => 41,  158 => 40,  151 => 36,  147 => 35,  142 => 33,  138 => 32,  134 => 31,  130 => 30,  126 => 29,  122 => 28,  119 => 27,  116 => 26,  111 => 25,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Plainte{% endblock %}

{% block body %}
        <h3><i class=\"fa fa-angle-right\"></i> <a href=\"{{ path('plainte_new') }}\"><button class=\"btn btn-success\">Nouvelle plainte</button></a></h3>
        <div class=\"row mt\">
          <div class=\"col-lg-12\">
            <div class=\"content-panel\">
             
              <section id=\"unseen\">
<table class=\"table table-bordered table-striped table-condensed\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Agent</th>
                <th>HeureDuDepot</th>
                <th>NomPlaignant</th>
                <th>NumeroPlaignant</th>
                <th>LieuDesFait</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
        {% for plainte in plaintes %}
            {% if app.user.groupe == plainte.groupe  %}
                <tr>
                    <td>{{ plainte.id }}</td>
                    <td>{{ plainte.agent }}</td>
                    <td>{{ plainte.heureDuDepot ? plainte.heureDuDepot|date('H:i:s') : '' }}</td>
                    <td>{{ plainte.nomPlaignant }}</td>
                    <td>{{ plainte.numeroPlaignant }}</td>
                    <td>{{ plainte.lieuDesFait }}</td>
                    <td>
                        <a href=\"{{ path('plainte_show', {'id': plainte.id}) }}\"><button type=\"button\" class=\"btn btn-warning\"><i class=\"fas fa-edit\"></i></button></a>
                        <a href=\"{{ path('plainte_edit', {'id': plainte.id}) }}\"><button type=\"button\" class=\"btn btn-primary\"><i class=\"far fa-eye\"></i></button></a>
                    </td>
                </tr>
        {% endif %}
        {% else %}
            <tr>
                <td colspan=\"8\">Aucune plainte pour le moment</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    
     </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        </div>

   
     </section>
 <!--main content end-->
    <!--footer start-->
 
{% endblock %}
", "plainte/index.html.twig", "C:\\appintranet\\templates\\plainte\\index.html.twig");
    }
}
