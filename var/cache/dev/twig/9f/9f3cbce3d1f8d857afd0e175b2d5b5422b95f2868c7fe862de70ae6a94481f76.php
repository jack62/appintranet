<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* rapport/index.html.twig */
class __TwigTemplate_42f3399373cc74887a4e4bda4b1e7c0dfa8dd56c068f11488458e941e5eeb8b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "rapport/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "rapport/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "rapport/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Liste des rapports";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
        <h3><i class=\"fa fa-angle-right\"></i> <a href=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rapport_new");
        echo "\"><button class=\"btn btn-success\">Nouveau Rapport</button></a></h3>
        <div class=\"row mt\">
          <div class=\"col-lg-12\">
            <div class=\"content-panel\">
              <section id=\"unseen\">
<table class=\"table table-bordered table-striped table-condensed\">
                  <thead>
                    <tr>
                       
                        <th>Agent Présent</th>
                        <th>Suspect</th>
                        <th>Date des faits</th>
                        <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rapports"]) || array_key_exists("rapports", $context) ? $context["rapports"] : (function () { throw new RuntimeError('Variable "rapports" does not exist.', 23, $this->source); })()));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["rapport"]) {
            // line 24
            echo "                    <tr>
                        
                        
                        <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rapport"], "agentSurInter", [], "any", false, false, false, 27), "html", null, true);
            echo "</td>
                        <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rapport"], "nom", [], "any", false, false, false, 28), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rapport"], "prenom", [], "any", false, false, false, 28), "html", null, true);
            echo "</td>
                        <td>";
            // line 29
            ((twig_get_attribute($this->env, $this->source, $context["rapport"], "dateFait", [], "any", false, false, false, 29)) ? (print (twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["rapport"], "dateFait", [], "any", false, false, false, 29), "d-m-Y H:i:s"), "html", null, true))) : (print ("")));
            echo "</td>
                        <td>
                            <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rapport_show", ["id" => twig_get_attribute($this->env, $this->source, $context["rapport"], "id", [], "any", false, false, false, 31)]), "html", null, true);
            echo "\"><button class=\"btn btn-theme\"><i class=\"fa fa-eye\"></i></button></a>
                            ";
            // line 32
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_ADMIN")) {
                // line 33
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rapport_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["rapport"], "id", [], "any", false, false, false, 33)]), "html", null, true);
                echo "\"><button class=\"btn btn-theme\"><i class=\"fa fa-edit\"></i></button></a>
                            ";
                // line 34
                echo twig_include($this->env, $context, "rapport/_delete_form.html.twig");
                echo "
                            ";
            }
            // line 36
            echo "                        </td>
                        
                    </tr> 
                    ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 40
            echo "                    <tr>
                        <td colspan=\"12\">Aucun Rapport actuellement</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rapport'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "                  
                  </tbody>
                  
                </table>
 </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        </div>

   
     </section>
 <!--main content end-->
    <!--footer start-->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "rapport/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 43,  180 => 40,  164 => 36,  159 => 34,  154 => 33,  152 => 32,  148 => 31,  143 => 29,  137 => 28,  133 => 27,  128 => 24,  110 => 23,  91 => 7,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Liste des rapports{% endblock %}

{% block body %}

        <h3><i class=\"fa fa-angle-right\"></i> <a href=\"{{ path('rapport_new') }}\"><button class=\"btn btn-success\">Nouveau Rapport</button></a></h3>
        <div class=\"row mt\">
          <div class=\"col-lg-12\">
            <div class=\"content-panel\">
              <section id=\"unseen\">
<table class=\"table table-bordered table-striped table-condensed\">
                  <thead>
                    <tr>
                       
                        <th>Agent Présent</th>
                        <th>Suspect</th>
                        <th>Date des faits</th>
                        <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    {% for rapport in rapports %}
                    <tr>
                        
                        
                        <td>{{ rapport.agentSurInter }}</td>
                        <td>{{ rapport.nom }} - {{ rapport.prenom }}</td>
                        <td>{{ rapport.dateFait ? rapport.dateFait|date('d-m-Y H:i:s') : '' }}</td>
                        <td>
                            <a href=\"{{ path('rapport_show', {'id': rapport.id}) }}\"><button class=\"btn btn-theme\"><i class=\"fa fa-eye\"></i></button></a>
                            {% if is_granted('ROLE_ADMIN') %}
                            <a href=\"{{ path('rapport_edit', {'id': rapport.id}) }}\"><button class=\"btn btn-theme\"><i class=\"fa fa-edit\"></i></button></a>
                            {{ include('rapport/_delete_form.html.twig') }}
                            {% endif %}
                        </td>
                        
                    </tr> 
                    {% else %}
                    <tr>
                        <td colspan=\"12\">Aucun Rapport actuellement</td>
                    </tr>
                {% endfor %}                  
                  </tbody>
                  
                </table>
 </div>
            <!-- /content-panel -->
          </div>
          <!-- /col-lg-4 -->
        </div>
        </div>

   
     </section>
 <!--main content end-->
    <!--footer start-->
{% endblock %}
", "rapport/index.html.twig", "C:\\appintranet\\templates\\rapport\\index.html.twig");
    }
}
