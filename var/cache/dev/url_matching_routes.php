<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/plainte' => [[['_route' => 'plainte_index', '_controller' => 'App\\Controller\\PlainteController::index'], null, ['GET' => 0], null, true, false, null]],
        '/plainte/new' => [[['_route' => 'plainte_new', '_controller' => 'App\\Controller\\PlainteController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/rapport' => [[['_route' => 'rapport_index', '_controller' => 'App\\Controller\\RapportController::index'], null, ['GET' => 0], null, true, false, null]],
        '/rapport/newRapport' => [[['_route' => 'rapport_new', '_controller' => 'App\\Controller\\RapportController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/saisie' => [[['_route' => 'saisie_index', '_controller' => 'App\\Controller\\SaisieController::index'], null, ['GET' => 0], null, true, false, null]],
        '/saisie/new' => [[['_route' => 'saisie_new', '_controller' => 'App\\Controller\\SaisieController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/inscription' => [[['_route' => 'security_registration', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'security_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/deconnexion' => [[['_route' => 'security_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/plainte/([^/]++)(?'
                    .'|(*:188)'
                    .'|/edit(*:201)'
                    .'|(*:209)'
                .')'
                .'|/rapport/([^/]++)(?'
                    .'|(*:238)'
                    .'|/(?'
                        .'|edit(*:254)'
                        .'|delete(*:268)'
                    .')'
                .')'
                .'|/saisie/([^/]++)(?'
                    .'|(*:297)'
                    .'|/edit(*:310)'
                    .'|(*:318)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        188 => [[['_route' => 'plainte_show', '_controller' => 'App\\Controller\\PlainteController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        201 => [[['_route' => 'plainte_edit', '_controller' => 'App\\Controller\\PlainteController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        209 => [[['_route' => 'plainte_delete', '_controller' => 'App\\Controller\\PlainteController::delete'], ['id'], ['POST' => 0], null, false, true, null]],
        238 => [[['_route' => 'rapport_show', '_controller' => 'App\\Controller\\RapportController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        254 => [[['_route' => 'rapport_edit', '_controller' => 'App\\Controller\\RapportController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        268 => [[['_route' => 'rapport_delete', '_controller' => 'App\\Controller\\RapportController::delete'], ['id'], ['POST' => 0], null, false, false, null]],
        297 => [[['_route' => 'saisie_show', '_controller' => 'App\\Controller\\SaisieController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        310 => [[['_route' => 'saisie_edit', '_controller' => 'App\\Controller\\SaisieController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        318 => [
            [['_route' => 'saisie_delete', '_controller' => 'App\\Controller\\SaisieController::delete'], ['id'], ['POST' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
