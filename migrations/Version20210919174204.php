<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210919174204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE avis_de_recherche (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, photo VARCHAR(255) DEFAULT NULL, motif VARCHAR(255) NOT NULL, contenue LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE casier (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_de_naissance DATETIME DEFAULT NULL, agent_createur VARCHAR(255) NOT NULL, partage TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plainte (id INT AUTO_INCREMENT NOT NULL, casier_id INT DEFAULT NULL, agent VARCHAR(255) NOT NULL, heure_du_depot DATETIME NOT NULL, nom_plaignant VARCHAR(255) NOT NULL, numero_plaignant VARCHAR(255) NOT NULL, lieu_des_fait LONGTEXT DEFAULT NULL, plainte LONGTEXT NOT NULL, INDEX IDX_D88CE90F643911C6 (casier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE saisie (id INT AUTO_INCREMENT NOT NULL, casier_id INT DEFAULT NULL, content LONGTEXT NOT NULL, prit_sur VARCHAR(255) NOT NULL, agent VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_158AB88643911C6 (casier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, matricule VARCHAR(255) NOT NULL, grade VARCHAR(255) DEFAULT NULL, service VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vol (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE plainte ADD CONSTRAINT FK_D88CE90F643911C6 FOREIGN KEY (casier_id) REFERENCES casier (id)');
        $this->addSql('ALTER TABLE saisie ADD CONSTRAINT FK_158AB88643911C6 FOREIGN KEY (casier_id) REFERENCES casier (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE plainte DROP FOREIGN KEY FK_D88CE90F643911C6');
        $this->addSql('ALTER TABLE saisie DROP FOREIGN KEY FK_158AB88643911C6');
        $this->addSql('DROP TABLE avis_de_recherche');
        $this->addSql('DROP TABLE casier');
        $this->addSql('DROP TABLE plainte');
        $this->addSql('DROP TABLE saisie');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE vol');
    }
}
