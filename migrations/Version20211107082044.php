<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211107082044 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD p_webhook LONGTEXT DEFAULT NULL, ADD r_webhook LONGTEXT DEFAULT NULL, ADD c_webhook LONGTEXT DEFAULT NULL, ADD s_webhook LONGTEXT DEFAULT NULL, ADD v_webhook LONGTEXT DEFAULT NULL, ADD a_webhook LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` DROP p_webhook, DROP r_webhook, DROP c_webhook, DROP s_webhook, DROP v_webhook, DROP a_webhook');
    }
}
